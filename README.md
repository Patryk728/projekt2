# Pobranie repozytorium:
    1. Utwórz nowy folder
    2. Uruchom Visual studio code 
    3. Otwórz terminal (ctrl + shift + `)
    4. w terminalu wklej - git clone https://Patryk728@bitbucket.org/Patryk728/projekt2.git
    
# Odpalenie projektu
    1. w terminalu wpisz:
        a) "cd projekt2" aby przejść do folderu z projektem
        b) "npm install"
        c) "ng serve"
    2. włącz przeglądarkę internetową
    3. wpisz adres: localhost:4200

#Odpalenie backendu
    1. Otwórz terminal (ctrl + shift + `)
    2. w terminalu wpisz:
                        a) cd back
                         b) npm install
    3. otwórz plik app.js
    4. Wpisz swój login i hasło (linie 11 i 12 w app.js)
    5. w terminalu wpisz: node app.js
    6. W przeglądarce wpisz: localhost:3000

# Dzwonienie
    1. W przeglądarce w localhost:4200 w lewym górnym rogu wpisz nr telefonu na jaki chcesz zadzwonić następnie kliknij przycisk: Zadzwoń

# Chat:
    1. W przeglądarce w localhost:4200 w lewe okienko (znajdujące się pod okienkiem do wpisywanie nr telefonu) wpisz nazwę użytkownika, w prawe okienko wpisz wiadomość, następnie kliknij przycisk: wyślij
 
#Uruchomienie PM2 (system oparty na systemie Linux)
    1. otwórz terminal (w pasku wyszukiwania wpisz cmd)
    2. przejdź do folderu back(cd back)
    3. wpisz: pm2 start testapp.config.js

#Uruchomienie playbook'a:
    1. otwórz terminal 
    2. wpisz: ansible-playbook playbook.yml -i inventory --connection=local -K
              pm2 status