import { Component, OnInit } from "@angular/core"
import { CallService } from "../call.service"

@Component({
  selector: "app-widget",
  templateUrl: "./widget.component.html",
  styleUrls: ["./widget.component.css"],
})
export class WidgetComponent implements OnInit {
  number: string
  validator = /(^[0-9]{9}$)/
  state: string = "waiting"
  interval: number
  userID: {}
  user: string = ""
  messages: {}
  message: string = ""

  constructor(private callService: CallService) {}

  ngOnInit() {
    this.getMessages();
    this.getUserID();
  }

  call() {
    if (this.isValidNumber()) {
      this.callService.placeCall(this.number)
      this.state = "ringing"
      this.callService.getCallId().subscribe(() => {
        this.checkStatus()
      })
    } else {
      console.info("Numer niepoprawny")
    }
  }

  isValidNumber(): Boolean {
    return this.validator.test(this.number)
  }

  checkStatus() {
    this.callService.getCallStatus().subscribe(state => {
      this.state = state
    })
  }

  getMessages()
  {
    this.messages = this.callService.getMessages()
  }

  getUserID()
  {
    this.userID = this.callService.getUserID();
  }

  send(){
    let message = {
      message: this.message,
      user: this.user
    }
    let user = {
    }
    this.callService.send(message);
    this.getMessages();
    this.getUserID();
  }
}
